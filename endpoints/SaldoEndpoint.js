const express = require("express");
const router = express.Router();
const db = require("../db");

// Endpoint untuk melihat seluruh isi tabel Saldo
router.get("/all", (req, res) => {
    db.query('SELECT * FROM Saldo', (err, result) => {
        if (err) {
            res.status(500).json({
                message: "Error retrieving saldo",
                status: "error"
            });
        } else {
            res.json({
                message: "Seluruh data saldo berhasil diambil",
                status: "success",
                data: result
            });
        }
    });
});

module.exports = router;
