const express = require("express");
const router = express.Router();
const db = require("../db");

router.post("/login", (req, res) => {

    const {Username, Password} = req.body;

    if (!Username || !Password) {

        return res
            .status(400)
            .json({message: "Username dan Password harus diisi", status: "error"});

    }

    db.query('SELECT * FROM Users WHERE Username = ? AND Password = ?', [
        Username, Password
    ], (err, result) => {

        if (err) {

            return res
                .status(500)
                .json({message: "Error checking credentials", status: "error"});

        }

        if (result.length > 0) {

            return res.json(
                {message: "Login berhasil", status: "success", user: result[0]}
            );

        } else {

            return res
                .status(401)
                .json({message: "Username atau Password salah", status: "error"});

        }

    });
});

// Endpoint untuk registrasi pengguna
router.post("/register", (req, res) => {
    const {username, password, userType} = req.body;

    // Validasi input
    if (!username || !password || !userType) {
        return res
            .status(400)
            .json(
                {message: "Username, password, dan userType harus diisi", status: "error"}
            );
    }

    // Cek apakah pengguna sudah terdaftar
    db.query(
        'SELECT * FROM Pengguna WHERE Username = ?',
        [username],
        (err, result) => {
            if (err) {
                return res
                    .status(500)
                    .json({message: "Error in database query", status: "error"});
            }

            // Jika pengguna sudah terdaftar
            if (result.length > 0) {
                return res
                    .status(409)
                    .json({message: "Username sudah terdaftar", status: "error"});
            }

            // Jika pengguna belum terdaftar, lakukan registrasi
            db.query(
                'INSERT INTO Pengguna (Username, Password, UserType) VALUES (?, ?, ?)',
                [
                    username, password, userType
                ],
                (err, result) => {
                    if (err) {
                        return res
                            .status(500)
                            .json({message: "Error in database query", status: "error"});
                    }

                    return res.json({message: "Registrasi berhasil", status: "success"});
                }
            );
        }
    );
});

router.delete("/delete/:username", (req, res) => {
    const usernameToDelete = req.params.username;

    // Validate if the username is provided
    if (!usernameToDelete) {
        return res
            .status(400)
            .json(
                {message: "Username parameter is required for deletion", status: "error"}
            );
    }

    // Your logic to delete the user from the database
    db.query(
        'DELETE FROM Pengguna WHERE Username = ?',
        [usernameToDelete],
        (err, result) => {
            if (err) {
                return res
                    .status(500)
                    .json({message: "Error in database query", status: "error"});
            }

            // Check if the user was found and deleted
            if (result.affectedRows === 0) {
                return res
                    .status(404)
                    .json({message: "User not found", status: "error"});
            }

            // User successfully deleted
            res.json({message: "User deleted successfully", status: "success"});
        }
    );
});

module.exports = router;
