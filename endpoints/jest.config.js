module.exports = {
    testEnvironment: 'node',
    transformIgnorePatterns: ['<rootDir>/node_modules/(?!(express-status-monitor)/)'],
    reporters: [
      'default',
      ['jest-html-reporters', {
        publicPath: './reports',
        filename: 'test-report.html',
        expand: true,
      }],
    ],
  };
  