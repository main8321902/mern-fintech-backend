const express = require("express");
const router = express.Router();
const db = require("../db");

// Endpoint untuk melihat seluruh isi tabel Barang
router.get("/all", (req, res) => {
    db.query('SELECT * FROM Barang', (err, result) => {
        if (err) {
            res.status(500).json({
                message: "Error retrieving barang",
                status: "error"
            });
        } else {
            res.json({
                message: "Seluruh data barang berhasil diambil",
                status: "success",
                data: result
            });
        }
    });
});

module.exports = router;
