const express = require("express");
const router = express.Router();
const db = require("../db");

// Endpoint untuk melihat seluruh isi tabel Transaksi
router.get("/all", (req, res) => {
    db.query('SELECT * FROM Transaksi', (err, result) => {
        if (err) {
            res.status(500).json({
                message: "Error retrieving transaksi",
                status: "error"
            });
        } else {
            res.json({
                message: "Seluruh data transaksi berhasil diambil",
                status: "success",
                data: result
            });
        }
    });
});

module.exports = router;
