const express = require("express");
const router = express.Router();
const db = require("../db");

// Endpoint untuk melihat seluruh isi tabel Users
router.get("/all", (req, res) => {
    db.query('SELECT * FROM Users', (err, result) => {
        if (err) {
            res.status(500).json({
                message: "Error retrieving users",
                status: "error"
            });
        } else {
            res.json({
                message: "Seluruh data pengguna berhasil diambil",
                status: "success",
                data: result
            });
        }
    });
});

module.exports = router;
