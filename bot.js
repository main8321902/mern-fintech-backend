// Menggunakan dotenv untuk membaca variabel lingkungan dari file .env
require("dotenv").config();

// Mengimpor modul Client dan Events dari @mengkodingan/ckptw
const { Client } = require("@mengkodingan/ckptw");
const { Events } = require("@mengkodingan/ckptw/lib/Constant");

// Impor modul db.js
const db = require("./db");

// Membuat instance bot dengan konfigurasi berdasarkan variabel lingkungan atau nilai default jika tidak ada
const bot = new Client({
    name: process.env.BOT_NAME ,  // Nama bot, bisa diatur di .env atau default "default-bot-name"
    prefix: process.env.BOT_PREFIX, 
    printQRInTerminal: process.env.PRINT_QR,  
    readIncommingMsg: process.env.READ_INCOMING_MSG  
});

// Menjalankan fungsi sekali saat bot siap (ClientReady)
bot.ev.once(Events.ClientReady, (m) => {
    console.log(`Whatsapp Telah Terhubung ${m.user.id}`);  // Menampilkan pesan saat bot terhubung
});

// Menjalankan fungsi setiap kali ada pesan baru (MessagesUpsert)
bot.ev.on(Events.MessagesUpsert, (m) => {
    console.log("log: ", JSON.stringify(m, null, 2));  // Menampilkan pesan baru dalam format JSON
});

// Menambahkan perintah bot untuk merespons "ping"
// bot.command('ping', async (ctx) => {
//     const commandText = ctx.message.body.toLowerCase();
//     if (commandText === '!ping') {
//         ctx.reply({ text: 'pong!' });
//     }
// });
bot.command(/ping/i, async (ctx) => ctx.reply({ text: 'pong!' }));

// Menambahkan fungsi yang akan dijalankan saat bot mendengar kata kunci 'help' atau 'menu'
bot.hears(/help|menu/i, async (ctx) => ctx.reply('test 1 2 3 beep boop...'));

// menambahkan perintah 'ayo'
bot.hears(/ayo/i, async (ctx) => ctx.reply('Saatnya belajar!'));

// Fungsi untuk memeriksa database
async function checkDatabase() {
  return new Promise((resolve, reject) => {
    db.query("SHOW TABLES", (error, results) => {
      if (error) {
        reject("Error saat mengambil tabel: " + error.message);
        return;
      }

      const tables = results.map(row => Object.values(row)[0]);
      resolve("Database terhubung. Tabel: " + tables.join(", "));
    });
  });
}

// Menambahkan perintah 'check_database'
bot.command('check_database', async (ctx) => {
    try {
        const dbStatus = await checkDatabase();
        ctx.reply({ text: dbStatus });
    } catch (error) {
        ctx.reply({ text: "Error: " + error });
    }
});

// Mengekspor instance bot untuk digunakan di file lain (seperti index.js)
module.exports = bot;
