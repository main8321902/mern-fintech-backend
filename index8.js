const express = require("express");
const basicAuth = require("express-basic-auth");
const logger = require("morgan");
const cookieParser = require('cookie-parser');
const db = require("./db");
const bot = require("./bot");
require("dotenv").config();

const app = express();
let statusMonitor;
if (process.env.NODE_ENV !== 'test') {
  statusMonitor = require("express-status-monitor");
} else {
  // Provide a dummy/mock implementation for testing
  statusMonitor = require("./mocks/express-status-monitor");
}
const basicAuthMiddleware = basicAuth({
  users: {
    [process.env.BASIC_AUTH_USERNAME]: process.env.BASIC_AUTH_PASSWORD,
  },
  challenge: true,
  unauthorizedResponse: "Unauthorized",
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Apply express-status-monitor middleware only in non-testing environment
if (process.env.NODE_ENV !== 'test') {
  app.use(statusMonitor({
    title: process.env.STATUS_MONITOR_TITLE || 'Express Status',
    path: '/',
  }));
}

app.get("/api/welcome", basicAuthMiddleware, function(req, res) {
  res.json({
    message: "Halo kami sedang membuat Backend-Rest-API",
    status: "success"
  });
});

app.get("/api/status_database", basicAuthMiddleware, function(req, res) {
  const status = {
    database: {
      connected: db.state === 'authenticated',
      host: db.config.host,
      user: db.config.user,
      database: db.config.database,
    }
  };

  db.query("SHOW TABLES", (error, results) => {
    if (error) {
      console.error('Error fetching tables: ' + error.stack);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }

    const tables = results.map(result => result[`Tables_in_${db.config.database}`]);
    status.database.tables = tables;

    res.json(status);
  });
});


app.use("/api/auth", require("./endpoints/AuthEndpoint"));
app.use("/api/users", require("./endpoints/UsersEndpoint"));
app.use("/api/barang", require("./endpoints/BarangEndpoint"));
app.use("/api/transaksi", require("./endpoints/TransaksiEndpoint"));
app.use("/api/saldo", require("./endpoints/SaldoEndpoint"));
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

const PORT = process.env.PORT || 8000;

// Export the app for testing purposes
module.exports = app;

if (process.env.NODE_ENV !== 'test') {
  app.listen(PORT, () => console.log(`Server is running at http://localhost:${PORT}`));
  bot.launch();
}
