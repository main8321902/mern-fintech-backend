const express = require("express");
const statusMonitor = require("express-status-monitor");
const basicAuth = require("express-basic-auth");

const app = express();

// Middleware untuk basic auth
// Contoh access http://admin:admin@mid.tachyon.net.id:8000/api/welcome
const basicAuthMiddleware = basicAuth({
  users: { 'admin': 'admin' },
  challenge: true, // Menyertakan header WWW-Authenticate untuk meminta kredensial
  unauthorizedResponse: "Unauthorized"
});

// Tambahkan express-status-monitor sebagai middleware
app.use(statusMonitor({
  title: 'Status Backend-Rest-API Joni',
  path: '/',
}));

// Gunakan basic auth middleware untuk endpoint /api/welcome
app.get("/api/welcome", basicAuthMiddleware, function(req, res) {
  res.json({
    message: "Halo kami sedang membuat Backend-Rest-API",
    status: "success"
  });
});

app.listen(8000, () => console.log("Server is Running at Port 8000"));
