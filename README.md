**MERN-FINTECH-BACKEND **
adalah backend untuk aplikasi fintech yang dibangun dengan teknologi MERN (MongoDB, Express.js, React, Node.js), walaupun dalam proyek ini menggunakan MySQL sebagai sistem manajemen basis data. Backend ini menyediakan berbagai fitur kunci, termasuk otentikasi pengguna, pemantauan kinerja server, dan integrasi dengan bot WhatsApp untuk notifikasi.

**Teknologi dan Stack:**
- **Express.js:** Kerangka kerja web untuk Node.js, digunakan untuk mengelola rute dan middleware API.
- **MySQL:** Sistem manajemen basis data relasional untuk menyimpan dan mengelola data aplikasi.
- **Node.js:** Runtime lingkungan JavaScript yang memungkinkan backend dijalankan menggunakan JavaScript.
- **WhatsApp Bot:** Bot untuk mengirimkan notifikasi dan layanan pesan berbasis WhatsApp.
- **Status Monitor:** Middleware untuk memantau kinerja server secara visual melalui antarmuka web.
- **Supertest:** Library pengujian HTTP untuk menguji endpoint API.

**Cara Menjalankan Proyek:**
1. Pastikan Node.js dan MySQL sudah terinstal di sistem.
2. Clone repositori ini ke lokal.
3. Buat salinan dari file `.env.example` dan ubah namanya menjadi `.env`. Isi konfigurasi yang sesuai, termasuk detail database dan kredensial bot WhatsApp.
4. Jalankan `npm install` untuk menginstal semua dependensi.
5. Jalankan `npm start` untuk memulai server backend.
6. Proyek ini akan berjalan di http://localhost:8000 (atau port yang telah dikonfigurasi).

**Menjalankan Unit Test:**
1. Pastikan lingkungan pengujian sudah disiapkan dalam file `.env` atau menggunakan konfigurasi khusus.
2. Jalankan `npm test` untuk menjalankan unit test.
3. Pantau keluaran untuk memastikan semua tes berhasil dijalankan.

**Penting:** Pastikan MySQL sudah terhubung dan konfigurasi bot WhatsApp valid sebelum menjalankan proyek ini.


### Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all Node dependencies pre-installed and Express will open a web preview.




Dengan proyek ini, Anda memiliki dasar backend solid untuk aplikasi fintech yang menggunakan MySQL sebagai basis data dan menyediakan fitur-fitur penting serta pengujian unit yang dapat diandalkan.
