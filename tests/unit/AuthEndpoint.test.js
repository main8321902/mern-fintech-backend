const request = require('supertest');
const app = require('../../index8');
const db = require('../../db'); // Import the database module

let server;

// Set up and tear down the server for testing
beforeAll(async () => {
  const port = process.env.TEST_SERVER_PORT || 8000;
  server = app.listen(port);
});

afterAll((done) => {
    server.close(() => {
      // Close the database connection after closing the server
      db.end((err) => {
        if (err) {
          console.error('Error closing the database connection:', err);
        }
        done();
      });
    });
  });
// Test suite for AuthEndpoint
describe('AuthEndpoint', () => {
  // Test user registration
  describe('POST /api/auth/register', () => {
    // Test case: Register a new user successfully
    it('should register a new user', async () => {
      const response = await request(app)
        .post('/api/auth/register')
        .send({
          username: 'tests_unit-newuser',
          password: 'newpassword',
          userType: 'Siswa'
        });

      expect(response.statusCode).toBe(200);
      expect(response.body.status).toBe('success');
      expect(response.body.message).toBe('Registrasi berhasil');
    });

    // Test case: Return 400 if username is already taken
    it('should return 400 if username is already taken', async () => {
      const response = await request(app)
        .post('/api/auth/register')
        .send({
          username: 'tests_unit-existinguser',
          password: 'newpassword',
          userType: 'Siswa'
        });

      expect(response.statusCode).toBe(409);
      expect(response.body.status).toBe('error');
      expect(response.body.message).toBe('Username sudah terdaftar');
    });

    // Test case: Return 400 if missing required fields
    it('should return 400 if missing required fields', async () => {
      const response = await request(app)
        .post('/api/auth/register')
        .send({
          // Missing userType
          username: 'missingfields',
          password: 'newpassword'
        });

      expect(response.statusCode).toBe(400);
      expect(response.body.status).toBe('error');
      expect(response.body.message).toBe('Username, password, dan userType harus diisi');
    });
  });

  // Test user deletion
  describe('DELETE /api/auth/delete/:username', () => {
    // Test case: Delete a registered user
    it('should delete a registered user', async () => {
      // Attempt to delete the registered user
      const deletionResponse = await request(app)
        .delete(`/api/auth/delete/${'tests_unit-newuser'}`);
  
      expect(deletionResponse.statusCode).toBe(200);
      expect(deletionResponse.body.status).toBe('success');
      expect(deletionResponse.body.message).toBe('User deleted successfully');
    });
  
    // Test case: Return 404 for deleting a non-existing user
    it('should return 404 for deleting a non-existing user', async () => {
      // Attempt to delete a non-existing user
      const deletionResponse = await request(app)
        .delete(`/api/auth/delete/${'nonexistentuser'}`);
  
      expect(deletionResponse.statusCode).toBe(404);
      expect(deletionResponse.body.status).toBe('error');
      expect(deletionResponse.body.message).toBe('User not found');
    });
  });
});
