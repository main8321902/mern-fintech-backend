// Import modul Express untuk membuat aplikasi web
// Install Express dengan perintah: npm install express
const express = require("express");

// Import modul express-status-monitor untuk memonitor status server
// Install express-status-monitor dengan perintah: npm install express-status-monitor
const statusMonitor = require("express-status-monitor");

// Import modul express-basic-auth untuk implementasi autentikasi dasar
// Install express-basic-auth dengan perintah: npm install express-basic-auth
// Contoh access http://admin:admin@mid.tachyon.net.id:8000/api/welcome
const basicAuth = require("express-basic-auth");

// Import modul morgan untuk logging HTTP requests
const morgan = require("morgan");

// Load konfigurasi dari file .env
require("dotenv").config();

// Membuat instance Express
const app = express();

// Middleware untuk autentikasi dasar (basic auth)

const basicAuthMiddleware = basicAuth({
  users: {
    [process.env.BASIC_AUTH_USERNAME]: process.env.BASIC_AUTH_PASSWORD,
  }, // Konfigurasi pengguna dan kata sandi
  challenge: true, // Menyertakan header WWW-Authenticate untuk meminta kredensial
  unauthorizedResponse: "Unauthorized", // Respon yang dikirim jika autentikasi gagal
});

// Middleware untuk logging menggunakan morgan dengan format 'short'
app.use(morgan('short'));

// Middleware express-status-monitor untuk memonitor status server
app.use(statusMonitor({
  title: process.env.STATUS_MONITOR_TITLE || 'Express Status', // Gunakan nilai dari variabel lingkungan atau nilai default jika tidak ada
  path: '/',
}));

// Menambahkan endpoint /api/welcome yang menggunakan autentikasi dasar
app.get("/api/welcome", basicAuthMiddleware, function(req, res) {
   // Mengirim respons JSON jika autentikasi berhasil
  res.json({
    message: "Halo kami sedang membuat Backend-Rest-API",
    status: "success"
  });
});

// Menjalankan server pada port yang diambil dari variabel lingkungan
const PORT = process.env.PORT || 8000;
app.listen(PORT, () => console.log(`Server is Running at Port ${PORT}`));
