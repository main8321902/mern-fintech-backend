require('dotenv').config();  // Menggunakan dotenv untuk membaca variabel lingkungan dari file .env

const mysql = require('mysql');

const db = mysql.createConnection({
    host: process.env.DB_HOST,      // Alamat host database
    port: process.env.DB_PORT,
    user: process.env.DB_USER,           // Nama pengguna database
    password: process.env.DB_PASSWORD,   // Kata sandi database
    database: process.env.DB_NAME   // Nama database yang akan digunakan
});

db.connect((err) => {
    if (err) {
        console.error('Error connecting to MySQL: ' + err.stack);
        return;
    }
    
    console.log('Database Berhasil Terhubung ' + db.threadId);
});

module.exports = db;
