// mocks/express-status-monitor.js

// This is a simple dummy/mock implementation for express-status-monitor
// You may need to adjust this based on the actual functions or objects used in your application

const createExpressStatusMonitor = () => {
    const middleware = (req, res, next) => {
      // Dummy implementation for the middleware
      // You may add any mock behavior needed for testing
      console.log('Mock express-status-monitor middleware called');
      next();
    };
  
    const gatherOsMetrics = () => {
      // Dummy implementation for gathering OS metrics
      // You may add any mock behavior needed for testing
      console.log('Mock express-status-monitor gathering OS metrics');
    };
  
    // Export the middleware and any other functions needed
    return {
      middleware,
      gatherOsMetrics,
      // Add any other functions or objects used by express-status-monitor
    };
  };
  
  module.exports = createExpressStatusMonitor();
  