const express = require("express");
const statusMonitor = require("express-status-monitor");

const app = express();

// Tambahkan express-status-monitor sebagai middleware
app.use(statusMonitor({
    title: 'Status Backend-Rest-API Joni',  // Set Nama
    path: '/',  // Set Path nya
  }));

 // Tambahkan satu endpoint
app.get("/api/welcome", function(req, res) {
    res.json({
        message: "Halo kami sedang membuat Backend-Rest-API",
        status: "success"
    });
});

app.listen(8000, () => console.log("Server is Running at Port 8000"));
