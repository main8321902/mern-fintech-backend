// Import modul Express untuk membuat aplikasi web
// Install Express dengan perintah: npm install express
const express = require("express");

// Import modul express-status-monitor untuk memonitor status server
// Install express-status-monitor dengan perintah: npm install express-status-monitor
const statusMonitor = require("express-status-monitor");

// Import modul express-basic-auth untuk implementasi autentikasi dasar
// Install express-basic-auth dengan perintah: npm install express-basic-auth
const basicAuth = require("express-basic-auth");

// Import modul morgan untuk logging HTTP requests
// Install morgan dengan perintah: npm install morgan
const morgan = require("morgan");

// Membuat instance Express
const app = express();

// Middleware untuk autentikasi dasar (basic auth)
// Contoh access http://admin:admin@mid.tachyon.net.id:8000/api/welcome
const basicAuthMiddleware = basicAuth({
  users: { 'admin': 'admin' }, // Konfigurasi pengguna dan kata sandi
  challenge: true, // Menyertakan header WWW-Authenticate untuk meminta kredensial
  unauthorizedResponse: "Unauthorized" // Respon yang dikirim jika autentikasi gagal
});

// Middleware untuk logging menggunakan morgan dengan format 'short'
app.use(morgan('short'));

// Middleware express-status-monitor untuk memonitor status server
app.use(statusMonitor({
  title: 'Status Backend-Rest-API Joni', // Judul status monitor
  path: '/', // Path status monitor
}));

// Menambahkan endpoint /api/welcome yang menggunakan autentikasi dasar
app.get("/api/welcome", basicAuthMiddleware, function(req, res) {
  // Mengirim respons JSON jika autentikasi berhasil
  res.json({
    message: "Halo kami sedang membuat Backend-Rest-API",
    status: "success"
  });
});

// Menjalankan server pada port 8000 dan mencetak pesan ke konsol
const PORT = 8000;
app.listen(PORT, () => console.log(`Server is Running at Port ${PORT}`));
